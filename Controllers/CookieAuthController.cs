using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// https://github.com/dotnet/aspnetcore/blob/main/src/Security/samples/Cookies/Controllers/AccountController.cs

namespace CookieAuth.Controllers
{
  public class LoginData
  {
    public string username { get; set; }
    public string password { get; set; }
  }

  [ApiController]
  [Route("[controller]")]
  public class CookieAuthController : ControllerBase
  {

    private readonly ILogger<WeatherForecastController> _logger;

    public CookieAuthController(ILogger<WeatherForecastController> logger)
    {
      _logger = logger;
    }

    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Login([FromBody] LoginData data)
    {

      Console.WriteLine($"Name: {data.username}, Password: {data.password}");

      var claims = new List<Claim>
      {
        new Claim("user", data.username),
        new Claim("role", "Member")
      };

      await HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, "Cookies", "user", "role")));

      var result = new
      {
        username = data.username,
        logged_in = true,
        role = "Member"
      };

      return Ok(result);
    }


    [HttpGet]
    [Route("greettwice")]
    public IActionResult GreetTwice(string name)
    {
      return Ok($"Hello, {name}! Hello, {name}!");
    }

    [Authorize]
    [Route("greet")]
    public IActionResult Greet(string name)
    {
      return Ok($"Hello, {name}!");
    }
  }
}