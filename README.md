# Basic Authentication in ASP.NET Core Web API using Cookies

## How to run the app

```bash
dotnet restore
dotnet watch run
```

## How to send requests using `curl`

```bash
# Should return `Hello, Johh! Hello, John!`; `GreetTwice` has anonymous access
curl -v http://localhost:5000/cookieauth/greet\?name\=john
# Should return 401 Unauthorized as `Greet` action has [Authorized] attribute
curl -v http://localhost:5000/cookieauth/greet\?name\=john
# Login using any username & password, no validation is done. Response should be a dict with username in it & a cookie.
curl -v -H "Content-Type:application/json" -d '{"username": "john", "password":"hello"}' http://localhost:5000/CookieAuth/login
# Copy the cookie from previous response into an environment variable. Example:
export COOKIE="auth_cookie=CfDJ8FTYsphtn1hDssk0XeniBF3bckeZag_IvJAAvPfvzGYpYX1bsoJj6k8qMlS99Rl6btM7mBQsHzzPhR3WKPZiRlh44JpiOh6kFCUwxIGkcaczbqihr-kD-LUFY5DnFISDUD3eF872aNBB3TSaECa6toFxF2f2k-cud5CDXi4os_clSW4ZriE6f7w7JqA3YgXO4YD-ileMTgRBrhQ9gg7FcQ5x5L9WEu3dpQaeYdiqAfy_xZUBK7blaDmhgRN74NCRxxHptPLW1M_PYgpkN0AYlgJ2sVL_mfrj5xWWb4ci4qacknXQODIy8DkKG4CpeEx4EhDO1EDtKISUO96aRwiXxSc; path=/; samesite=none; httponly"
# Now send a request to `Greet` action again, this time include the cookie. Should return `Hello, John!`.
curl -v --cookie $COOKIE http://localhost:5000/cookieauth/greet\?name\=john
```
